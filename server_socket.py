import socket
import threading

HEADER = 64  #tamaño del paqueta 64 bytes 
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NES CONNECTION] {addr} connected")
    #ahora estructuramos, el mensaje que esta llegando tendra un ancho determinado y un formato
    #primero tenemos que chequear cuanteos bytes estamos recibiendo
    connected = True # esta variable me permite mantener la conexion
    while connected:
        msg_lenght = conn.recv(HEADER).decode(FORMAT) #largo del mensaje
        if msg_lenght:
            msg_lenght = int(msg_lenght)
            msg = conn.recv(msg_lenght).decode(FORMAT) #docodificamos formato utf8
            if msg == DISCONNECT_MESSAGE: # si el mensaje recibido por el cliente es , entonces nos desconectamos
                connected = False

            print(f"[{addr}] {msg}")
            conn.send("Mensaje recibido".encode(FORMAT))
    conn.close()
    





def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept() # devuelve un objeto socket donde mantiene la coneccion y ademas nos devuelve el addreees de la coneccion que se guarda en addr, conn permite establecer comunocacion bidireccional con el cliente
                                    #handshacking sirve como estrategia de seguridad, el cliente manda la mac address y una vez que la mac addrss esta verificada se manda la data

        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE_CONNECTIONS] {threading.activeCount() - 1}")
    
print("[STARTING] Server is running...")
start()

