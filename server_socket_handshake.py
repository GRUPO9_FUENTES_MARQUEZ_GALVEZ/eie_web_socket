from uuid import getnode as get_mac_address
import socket
import threading

HEADER = 64  #tamaño del paqueta 64 bytes 
PORT = 3074
SERVER = '158.251.91.68' #ip de la raspberry
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
MAC = '08:00:27:37:32:05'
#convertimos el socket en un objeto, AF_INET es conexion ipv4, sock_stream stremear datos (trasmitir)
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#linkeamos el socket a la ip donde queremos que este escuchando
server.bind(ADDR) #bind es un metodo definido dentro de la libreria socket para objetos tipo socket

#funcion capaz de manejar multiples conexiones en cada hilo del procesador
def handle_client(conn, addr):
    print(f"[NES CONNECTION] {addr} connected")
    #ahora estructuramos, el mensaje que esta llegando tendra un ancho determinado y un formato
    #primero tenemos que chequear cuanteos bytes estamos recibiendo
    connected = True # esta variable me permite mantener la conexion
    msg_lenght = conn.recv(HEADER).decode(FORMAT)
    if msg_lenght:
                msg_lenght = int(msg_lenght)
                msg = conn.recv(msg_lenght).decode(FORMAT)
    if msg == MAC:
        
        while connected:
            
            
            msg_lenght = conn.recv(HEADER).decode(FORMAT) #largo del mensaje
            if msg_lenght:
                msg_lenght = int(msg_lenght)
                msg = conn.recv(msg_lenght).decode(FORMAT) #docodificamos formato utf8
                if msg == DISCONNECT_MESSAGE: # si el mensaje recibido por el cliente es , entonces nos desconectamos
                    connected = False

                print(f"[{addr}] {msg}")
                conn.send("Mensaje recibido".encode(FORMAT))
        conn.close()
    conn.close()



#funcion que esta escuchando y que cuando reciba una nueva conexion, pasas esa conexion a la funcion handle client y se este ejecutando en un hilo
def start():
    server.listen() # iniciamos escucha del servidor
    print(f"[LISTEN] Server is listening on address {ADDR}")
    
    #un ciclo infinito que esta siempre escuchando y aceptando peticiones
    while True:
        conn, addr = server.accept() # devuelve un objeto socket donde mantiene la conexión y ademas nos devuelve el address de la conexion que se guarda en addr, conn permite establecer comunocacion bidireccional con el cliente
                                    #handshacking sirve como estrategia de seguridad, el cliente manda la mac address y una vez que la mac addrss esta verificada se manda la data

        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE_CONNECTIONS] {threading.activeCount() - 1}")
    

print("[STARTING] Server is running...")
start()


