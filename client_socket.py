import socket
import threading
from getmac import get_mac_address as gma

HEADER = 64  #tamaño del paqueta 64 bytes (largo total)
PORT = 3074
SERVER = '158.251.91.68'
ADDR = (SERVER, PORT) #tuple
FORMAT = 'utf-8' #codificacion formato utf 8
DISCONNECT_MESSAGE = 'DISCONNECT!'

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #socket tipo ipv4
#asociamos el socket al cliente
client.connect(ADDR)

#funcion que le pasamos un parametro y lo envia
def send(msg):
    message =msg.encode(FORMAT) #codificamos el mensaje en formato utf8
    msg_length = len(message) #obtenemos el largo del mensaje como un valor de tipo entero
    send_length = str(msg_length).encode(FORMAT) #lo convierto a string y lo codifico en formato utf 8
    # en el servidor tenemos configurado un largo preestablecido
    send_length += b' '*(HEADER-len(send_length)) # formato binario, espacio, y la mult por el header menos el largo de la variable codificada 
    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT)) # con esto vemos si realmente estamos recibiendo algo del servidor

send('hello')
input() #para que se ejecute este input se debe apretar cualquier tecla
send(gma())
input()
test_data = list()
test_data = [17,89]
data_socket = 'temperatura='+str(test_data[0])+','+'humedad='+str(test_data[1])
send(data_socket)
input()
send(DISCONNECT_MESSAGE)
